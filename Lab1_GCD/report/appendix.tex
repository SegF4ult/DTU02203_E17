\newpage
\begin{appendices}

\section{Design Code}\label{app:design_code}

\subsection{GCD Entity Declaration}
\begin{lstlisting}[caption=GCD Entity, label=lst:gcd_entity]
--
--  Title      :  FSMD implementation of GCD
--
--  Developers :  Jens Sparsø, Rasmus Bo Sørensen and
--             :  Mathias Møller Bruhn
--
--  Purpose    :  This is a FSMD (finite state machine with datapath) 
--             :  implementation the GCD circuit
--
--  Revision   :  02203 fall 2017 v.4
--
--  Modified By: Xander Gregory Bos, Rafael Baron
--  Notes      : Separated entity declaration from architectures,
--		 to promote re-use and easy reimplementation of designs.
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity gcd is
	port (clk:   in std_logic;			-- The clock signal.
	      reset: in std_logic;			-- Reset the module.
	      req:   in std_logic;			-- Input operand / Start computation.
	      AB:    in unsigned(15 downto 0);		-- The two operands.
	      ack:   out std_logic;			-- Computation is complete.
	      C:     out unsigned(15 downto 0));	-- The result.
end gcd;
\end{lstlisting}

\subsection{GCD Test Environment}
\begin{lstlisting}[caption=GCD Testbench, label=lst:gcd_env]
--
--  Title      :  Environment for the GCD module
--             
--  Developers :  Jens Sparsø, Rasmus Bo Sørensen and Mathias Møller Bruhn
--             
--  Purpose    :  This design is an environment for the GCD module.
--             :  It provides input signals to a GCD module and
--             :  collects the result of a computation. It should be
--             :  connected to a GCD module only in a testbench.
--             
--  Revision   : 02203 fall 2017 v.5.0
--
--  Modified By: Xander Gregory Bos, Rafael Baron
--  Notes      : Testbench was modified to accomodate our statemachine
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Entity declaration for the environment. It mirrors the entity declaration for
-- the GCD module so that they can be connected.
entity env is
	generic(
		       period: TIME := 30 ns);
	port(
		    clk:   out std_logic;             -- The clock signal.
		    reset: in std_logic;              -- Reset the module.
		    req:   out std_logic;             -- Start computation.
		    AB:    out unsigned(15 downto 0); -- The two operands. One at at time
		    ack:   in std_logic;              -- Computation is complete.
		    C:     in unsigned(15 downto 0)); -- The result.
end env;

-- Behaviour model of the environment. It applies operands and the "req" signal
-- to a GCD module and when the computation is complete, it collects the result.
architecture behaviour of env is
    -- This type will indicate the state of the statemachine.
	type StateType is (INPUT_A, DONE_A, INPUT_B, CALC_C, DONE_ALL);

    -- Two signals to hold states.
	signal current_state, next_state: StateType;

    -- Result register.
	signal R: unsigned(15 downto 0);

    -- Enable signal for the result register.
	signal ld_R: std_logic;

    -- Internal clock signal
	signal clk_gen : std_logic := '0';

begin
    -- Combinatorial part of the statemachine. It is responsible for the
    -- computation of the next state. It must be sensitive to the state
    -- and all inputs used to compute the next state.
	process (current_state, ack)
		type Operands is array (0 TO 4) of NATURAL;
	-- Change numbers here if you what to run different tests
		variable a_Operands : Operands := (91, 32768, 49, 29232, 25);
		variable b_Operands : Operands := (63,   272, 98,   488,  5);
		variable c_results  : Operands := ( 7,    16, 49,     8,  5);
		--variable c_results  : Operands := (91, 32768, 49, 29232, 25);
		variable test_number : INTEGER := 0;
	begin
		case current_state is

			when INPUT_A =>
				req  <= '1';
				AB   <= TO_UNSIGNED(a_Operands(test_number),AB'length);
		-- The a operant is converted to a std_logic_vector

				if ack = '0' then              -- wait until finish signal ack is one
					next_state <= INPUT_A;
				else                           -- then set next_state to DONE
					next_state <= DONE_A;
				end if;

			when DONE_A =>
				req  <= '0';                   -- Phase 3 of handshaking protocol.
				AB   <= (others => 'X');       -- remove A

				if ack = '1' then              -- wait until GCD module finishes the
					next_state <= DONE_A;      -- handshake protocol,
				else                           -- then start a new computation.
					next_state <= INPUT_B;
				end if;

			when INPUT_B =>
				req  <= '1';
				AB   <= TO_UNSIGNED(b_Operands(test_number),AB'length);
		-- The b operant is converted to a std_logic_vector
				ld_R <= '1';                   -- Enable result register.
				next_state <= CALC_C;

			when CALC_C =>	
				if ack = '0' then              -- wait until finish signal ack is one
					next_state <= CALC_C;
				else                           -- then set next_state to DONE
					next_state <= DONE_ALL;
		--req  <= '0';                   -- Phase 3 of handshaking protocol.
				end if;

			when DONE_ALL =>
				req  <= '0';                   -- Phase 3 of handshaking protocol.
				AB   <= (others => 'X');       -- remove B
				ld_R <= '0';                   -- Disable result register.
				assert R = TO_UNSIGNED(c_results(test_number),C'length) report "Wrong result!" severity failure;
				if ack = '1' then              -- wait until GCD module finishes the
					next_state <= DONE_ALL;      -- handshake protocol,
				else                           -- then start a new computation.
					if test_number < 4 then
						test_number := test_number + 1;
					else
						test_number := 0;
						report "Test passed" severity failure;
					end if;
					next_state <= INPUT_A;
				end if;

		end case;
	end process;

    -- This process assigns next_state to current_state. It implements the
    -- state holding registers in the statemachine.
	process(clk_gen, ld_R, reset)
	begin
		if rising_edge(clk_gen) then
			if ld_R = '1' then
				R <= C;
			end if;

			if reset = '1' then
				current_state <= INPUT_A;         -- Reset to initial state
			else
				current_state <= next_state;      -- go to next_state
			end if;
		end if;
	end process;

    -- Clock generation (simulation use only)
	process
	begin
		clk_gen <= '1', '0' after period/2;
		wait for period;
	end process;
	clk <= clk_gen;
end behaviour;
\end{lstlisting}

\subsection{GCD RTL Design}\label{app:gcd_rtl}
\begin{lstlisting}[caption=GCD RTL Description of FSMD, label=lst:gcd_rtl]
--
--  Title      :  RTL-level FSMD design of the GCD component
--             :
--  Developers :  Xander Gregory Bos, Rafael Baron
--             :
--  Purpose    :  This design contains the FSM with Datapath 
--             :  used to implement Euler's GCD algorithm
--             :
--  Note       :  The design is modeled after a 2-process sequential model:
--             :  - One process handles register updates
--             :  - One process models the FSM and CL
--             :  
--  Revision   :  02203 fall 2017 v.1.0
--

architecture FSMD of gcd is

type state_type is (INPUT_A, DONE_A, INPUT_B, CALC_C, DONE_ALL); -- Input your own state names

signal state, state_next : state_type;
signal reg_a, reg_a_next : unsigned(15 downto 0) := (others=> '0');
signal reg_b, reg_b_next : unsigned(15 downto 0) := (others=> '0');

begin

-- Registers
seq: process (clk, reset)
begin
	if reset='1' then
		state <= INPUT_A;
		reg_a <= (others=> '0');
		reg_b <= (others=> '0');
	elsif rising_edge(clk) then
		state <= state_next;
		reg_a <= reg_a_next;
		reg_b <= reg_b_next;
	end if;
end process seq;

-- Combinatorial logic
CL: process (req,AB,state,reg_a,reg_b,reset)
begin

   case state is
	
	when INPUT_A =>
		if req = '1' then
			reg_a_next <= AB;
			ack <= '1';			
			state_next <= DONE_A;
		elsif req = '0' then
			state_next <= INPUT_A;
		end if;
	
	when DONE_A =>		
		if req = '0' then
			ack <= '0';
			state_next <= INPUT_B;
		elsif req = '1' then
			state_next <= DONE_A;
		end if;
	
	when INPUT_B =>
		if req='1' then
			reg_b_next <= AB;
			state_next <= CALC_C;
		else
			state_next <= INPUT_B;
		end if;	

	when CALC_C =>
		state_next <= CALC_C;
		reg_a_next <= reg_a;
		reg_b_next <= reg_b;
		ack <= '0';

		if reg_a /= reg_b then
			if reg_a > reg_b then
				reg_a_next <= reg_a - reg_b;
			else
				reg_b_next <= reg_b - reg_a;
			end if;
		else
			C <= reg_a;
			ack <= '1';
			state_next <= DONE_ALL;
		end if;
	
	when DONE_ALL =>
		if req = '0' then
			ack <= '0';
			state_next <= INPUT_A;
		elsif req='1' then
			state_next <= DONE_ALL;
		end if;

   end case;
end process CL;
end FSMD;
\end{lstlisting}

\subsection{GCD RTL Design Optimized}
\begin{lstlisting}[caption=GCD RTL Description of FSMD Optimized, label=lst:gcd_rtl_opt]
--
--  Title      :  RTL-level FSMD design of the GCD component
--             :
--  Developers :  Xander Gregory Bos, Rafael Baron
--             :
--  Purpose    :  This design contains the FSM with Datapath 
--             :  used to implement Euler's GCD algorithm
--             :
--  Note       :  Modelled after a 2-process sequential model:
--             :  - One process handles register updates
--             :  - One process models the FSM and CL
--             :  
--  Revision   :  02203 fall 2017 v.1.1
--

architecture RTL of gcd is

type state_type is (INPUT_A, DONE_A, INPUT_B, CALC_C, DONE_ALL);
signal state, state_next : state_type;
signal reg_a, reg_a_next : unsigned(15 downto 0) := (others=> '0');
signal reg_b, reg_b_next : unsigned(15 downto 0) := (others=> '0');

begin

-- Registers
seq: process (clk, reset)
begin
	if reset='1' then
		state <= INPUT_A;
		reg_a <= (others=> '0');
		reg_b <= (others=> '0');
	elsif rising_edge(clk) then
		state <= state_next;
		reg_a <= reg_a_next;
		reg_b <= reg_b_next;
	end if;
end process seq;

-- Combinatorial logic
CL: process (req,AB,state,reg_a,reg_b,reset)
begin
    -- default signal assignments
    reg_a_next <= reg_a;
    reg_b_next <= reg_b;
    ack <= '0';
    C <= (others=>'0');

   case state is
	when INPUT_A =>
		if req = '1' then
			reg_a_next <= AB;
			ack <= '1';			
			state_next <= DONE_A;
		elsif req = '0' then
			state_next <= INPUT_A;
		end if;
	
	when DONE_A =>		
		if req = '0' then
			ack <= '0';
			state_next <= INPUT_B;
		elsif req = '1' then
			state_next <= DONE_A;
		end if;
	
	when INPUT_B =>
		if req='1' then
			reg_b_next <= AB;
			state_next <= CALC_C;
		else
			state_next <= INPUT_B;
		end if;	

	when CALC_C =>
		state_next <= CALC_C;

		if reg_a /= reg_b then
			if reg_a > reg_b then
				reg_a_next <= reg_a - reg_b;
			else
				reg_b_next <= reg_b - reg_a;
			end if;
		else
			state_next <= DONE_ALL;
		end if;
	
	when DONE_ALL =>
	    ack <= '1';
	    C <= reg_a;
		if req = '0' then
			state_next <= INPUT_A;
		elsif req='1' then
			state_next <= DONE_ALL;
		end if;

   end case;
end process CL;
end RTL;
\end{lstlisting}

\subsection{GCD Structural Architecture}
\begin{lstlisting}[caption=GCD Structural Architecture, label=lst:gcd_structural]
--
--  Title      :  Structural description for GCD
--             
--  Developers :  Xander Gregory Bos, Rafael Baron
--             
--  Purpose    :  This design uses submodules 
--             :  to implement Euler's GCD algorithm
--             
--  Note       :  
--               
--  Revision   :  02203 fall 2017 v.1.0
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

architecture structural of gcd is
    component gcd_fsm
        Port (
      -- Top-level signals
        clk: IN std_logic;
        reset: IN std_logic; 
        req: IN std_logic;
        ack: OUT std_logic;
      -- Input from Datapath
        N: IN std_logic;
        Z: IN std_logic;
      -- Control to Datapath
        ABorALU: OUT std_logic;
        LDA: OUT std_logic;
        LDB: OUT std_logic;
        FN: OUT std_logic_vector(1 downto 0)
      );
    end component;
    
    component gcd_datapath
        Port(
      -- Top-Level signals
      clk: IN std_logic;
      reset: IN std_logic;
      
      AB: IN unsigned(15 downto 0);
      C: OUT unsigned(15 downto 0);
      -- Output to Datapath
      N: OUT std_logic;
      Z: OUT std_logic;
    -- Control from FSM
      ABorALU: IN std_logic;
      LDA: IN std_logic;
      LDB: IN std_logic;
      FN: IN std_logic_vector(1 downto 0) 
        );
    end component;
    
    signal ABorALU, Neg, Zero, LDA, LDB: std_logic;
    signal FN: std_logic_vector(1 downto 0);

begin
    fsm : gcd_fsm PORT MAP (clk=>clk,  reset=>reset, req=>req, ack=>ack, N=>Neg, Z=>Zero, ABorALU=>ABorALU, LDA=>LDA, LDB=>LDB, FN=>FN);
    datapath: gcd_datapath PORT MAP (clk=>clk, reset=>reset, AB=>AB, C=>C, N=>Neg, Z=>Zero, ABorALU=>ABorALU, LDA=>LDA, LDB=>LDB, FN=>FN);

end structural;
\end{lstlisting}

\subsection{GCD FSM}
\begin{lstlisting}[caption=GCD FSM Component, label=lst:gcd_fsm]
--
--  Title      :  FSM Design for Structural implementation
--             :  of GCD
--             
--  Developers :  Xander Gregory Bos, Rafael Baron
--             
--  Purpose    :  This design contains the FSM
--             
--  Note       :
--               
--  Revision   :  02203 fall 2017 v.1.0
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity gcd_fsm is
  Port (
  -- Top-level signals
    clk: in std_logic;
    reset: in std_logic; 
    req: in std_logic;
    ack: out std_logic;
  -- Input from Datapath
    N: in std_logic;
    Z: in std_logic;
  -- Control to Datapath
    ABorALU: out std_logic;
    LDA: out std_logic;
    LDB: out std_logic;
    FN: out std_logic_vector(1 downto 0)
  );
end gcd_fsm;

architecture Behavioral of gcd_fsm is

type state_type is (INPUT_A, DONE_A, INPUT_B, CALC_C, DONE_ALL);
signal state, state_next : state_type := INPUT_A;
signal FN_reg, FN_next : std_logic_vector(1 downto 0) := "00";
-- FN "00" A-B
-- FN "01" B-A
-- FN "10" Pass A
-- FN "11" Pass B

begin

-- Registers
reg: process(clk, reset)
begin
	if reset='1' then
		state <= INPUT_A;
	elsif rising_edge(clk) then
		state <= state_next;
		FN <= FN_next;
		FN_reg <= FN_next;
	end if;
end process reg;

-- Combinatorial logic
cl: process(reset, req, N, Z)
begin
	-- Default signal assignments
	ABorALU <= '0';
	LDA <= '0';
	LDB <= '0';
	ack <= '0';
	state_next <= state;
	FN_next <= FN_reg;

	case state is
		when INPUT_A =>
			if req='1' then
				ack <= '1';
				LDA <= '1';
				state_next <= DONE_A;
			end if;

		when DONE_A =>
			if req='0' then
				state_next <= INPUT_B;
			end if;

		when INPUT_B =>
			if req='1' then
				state_next <= CALC_C;
				LDB <= '1';
--				ack<='1';
			end if;
		
		when CALC_C =>
			ABorALU <= '1';
		
			if FN_reg="00" and N='0' then
				LDA<='1';
			elsif FN_reg="00" and N='1' then
				FN_next <= "01";
			elsif FN_reg="01" and N='0' then
				LDB<='1';
			elsif FN_reg="01" and N='1' then
				FN_Next <= "00";
			end if;

			if Z='1' and FN_reg="00" then
				state_next <= DONE_ALL;
				FN_next <= "11"; -- PASS B;
				ack <= '1';
			elsif Z='1' and FN_reg="01" then
				state_next <= DONE_ALL;
				FN_next <= "10"; -- Pass A;
				ack <= '1';
			end if;

		when DONE_ALL =>
			ABorALU <= '1';
			ack <= '0';

			if req='0' then
				state_next <= INPUT_A;
			end if;
	end case;
end process cl;

end Behavioral;
\end{lstlisting}

\subsection{GCD Datapath with existing components}
\begin{lstlisting}[caption=GCD Datapath Component, label=lst:gcd_datapath]
--
--  Title      :  Datapath Design for Structural implementation
--             :  of GCD
--             
--  Developers :  Xander Gregory Bos, Rafael Baron
--             
--  Purpose    :  This design contains the Datapath
--             
--  Note       :
--               
--  Revision   :  02203 fall 2017 v.1.0
--
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY gcd_datapath IS
    PORT (
   		-- top-level signals 
		   clk:		IN STD_LOGIC;
    		   reset:	IN STD_LOGIC;
		   AB:		IN  UNSIGNED(15 downto 0);
		   C	  : OUT unsigned(15 downto 0);
    		-- input from FSM
	           ABorALU:	IN  STD_LOGIC;
		   LDA:		IN  STD_LOGIC;
		   LDB:		IN  STD_LOGIC;
		   fn :		IN  std_logic_vector(1 downto 0);
    		-- output to FSM
		   N :		OUT  STD_LOGIC;
		   Z :		OUT  STD_LOGIC
		   );
END gcd_datapath;

ARCHITECTURE datapath OF gcd_datapath IS
COMPONENT mux IS
    PORT (data_in1:  IN  unsigned(15 DOWNTO 0);	-- Inputs.
          data_in2:  IN unsigned(15 DOWNTO 0);
          s       :  IN std_logic;				-- Select signal.
          data_out:  OUT  unsigned(15 DOWNTO 0)	-- Output.
          );
END COMPONENT;

COMPONENT reg IS
    PORT (clk:      IN  std_logic;				-- Clock signal.
          en:       IN  std_logic;				-- Enable signal.
          data_in:  IN  unsigned(15 DOWNTO 0);	-- Input data.
          data_out: OUT unsigned(15 DOWNTO 0));	-- Output data.
END COMPONENT;

COMPONENT alu IS
    PORT (A, B:     IN unsigned(15 DOWNTO 0);			-- Input operands.
          fn:       IN std_logic_vector(1 DOWNTO 0); 	-- Function.
          C:        OUT unsigned(15 DOWNTO 0);			-- Result.
          Z:        OUT std_logic;          			-- Result = 0 flag.
          N:        OUT std_logic);         			-- Result neg flag.
END COMPONENT;

COMPONENT tri IS
    PORT (data_in: IN unsigned(15 DOWNTO 0);
    	  data_out: OUT unsigned(15 DOWNTO 0));
END COMPONENT;

SIGNAL reg_a, reg_b, Y, C_int : unsigned(15 downto 0) := (others=> '0');

BEGIN
	
mux1: mux port map (data_in1=>AB, data_in2=>Y, s=>ABorALU, data_out=>C_int);
regA: reg port map (clk=>clk, en=>LDA, data_in=>C_int, data_out=>reg_a);
regB: reg port map (clk=>clk, en=>LDB, data_in=>C_int, data_out=>reg_b);
ALU1: ALU port map (A=>reg_a, B=>reg_b, fn=>fn, C=>Y, Z=>Z, N=>N);
tribuff: tri port map(data_in=>C_int, data_out=>C);

END datapath;
\end{lstlisting}

\end{appendices}
--
--  Title      :  Environment for the GCD module
--             :
--  Developers :  Jens Spars�, Rasmus Bo S�rensen and Mathias M�ller Bruhn
--             :
--  Purpose    :  This design is an environment for the GCD module. It provide
--             :  input signals to a GCD module and collects the result of a
--             :  computation. It should be connected to a GCD module only in a
--             :  testbench.
--             :
--  Revision   : 02203 fall 2017 v.5.0
--
--  Modified By: Xander Gregory Bos, Rafael Baron
--  Notes      : Testbench was modified to accomodate our statemachine
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--------------------------------------------------------------------------------
-- Entity declaration for the environment. It mirrors the entity declaration for
-- the GCD module so that they can be connected.
--------------------------------------------------------------------------------

entity env is
	generic(
		       period: TIME := 30 ns);
	port(
		    clk:   out std_logic;             -- The clock signal.
		    reset: in std_logic;              -- Reset the module.
		    req:   out std_logic;             -- Start computation.
		    AB:    out unsigned(15 downto 0); -- The two operands. One at at time
		    ack:   in std_logic;              -- Computation is complete.
		    C:     in unsigned(15 downto 0)); -- The result.
end env;

--------------------------------------------------------------------------------
-- Behaviour model of the environment. It applies operands and the "req" signal
-- to a GCD module and when the computation is complete, it collects the result.
--------------------------------------------------------------------------------

architecture behaviour of env is
    -- This type will indicate the state of the statemachine.
	type StateType is (INPUT_A, DONE_A, INPUT_B, CALC_C, DONE_ALL);

    -- Two signals to hold states.
	signal current_state, next_state: StateType;

    -- Result register.
	signal R: unsigned(15 downto 0);

    -- Enable signal for the result register.
	signal ld_R: std_logic;

    -- Internal clock signal
	signal clk_gen : std_logic := '0';

begin
    -- Combinatorial part of the statemachine. It is responsible for the
    -- computation of the next state. It must be sensitive to the state
    -- and all inputs used to compute the next state.
	process (current_state, ack)
		type Operands is array (0 TO 4) of NATURAL;
	-- Change numbers here if you what to run different tests
		variable a_Operands : Operands := (91, 32768, 49, 29232, 25);
		variable b_Operands : Operands := (63,   272, 98,   488,  5);
		variable c_results  : Operands := ( 7,    16, 49,     8,  5);
		--variable c_results  : Operands := (91, 32768, 49, 29232, 25);
		variable test_number : INTEGER := 0;
	begin
		case current_state is

			when INPUT_A =>
				req  <= '1';
				AB   <= TO_UNSIGNED(a_Operands(test_number),AB'length);
		-- The a operant is converted to a std_logic_vector

				if ack = '0' then              -- wait until finish signal ack is one
					next_state <= INPUT_A;
				else                           -- then set next_state to DONE
					next_state <= DONE_A;
				end if;

			when DONE_A =>
				req  <= '0';                   -- Phase 3 of handshaking protocol.
				AB   <= (others => 'X');       -- remove A

				if ack = '1' then              -- wait until GCD module finishes the
					next_state <= DONE_A;      -- handshake protocol,
				else                           -- then start a new computation.
					next_state <= INPUT_B;
				end if;

			when INPUT_B =>
				req  <= '1';
				AB   <= TO_UNSIGNED(b_Operands(test_number),AB'length);
		-- The b operant is converted to a std_logic_vector
				ld_R <= '1';                   -- Enable result register.
				next_state <= CALC_C;

			when CALC_C =>	
				if ack = '0' then              -- wait until finish signal ack is one
					next_state <= CALC_C;
				else                           -- then set next_state to DONE
					next_state <= DONE_ALL;
		--req  <= '0';                   -- Phase 3 of handshaking protocol.
				end if;

			when DONE_ALL =>
				req  <= '0';                   -- Phase 3 of handshaking protocol.
				AB   <= (others => 'X');       -- remove B
				ld_R <= '0';                   -- Disable result register.
				assert R = TO_UNSIGNED(c_results(test_number),C'length) report "Wrong result!" severity failure;
				if ack = '1' then              -- wait until GCD module finishes the
					next_state <= DONE_ALL;      -- handshake protocol,
				else                           -- then start a new computation.
					if test_number < 4 then
						test_number := test_number + 1;
					else
						test_number := 0;
						report "Test passed" severity failure;
					end if;
					next_state <= INPUT_A;
				end if;

		end case;
	end process;

    -- This process assigns next_state to current_state. It implements the
    -- state holding registers in the statemachine.
	process(clk_gen, ld_R, reset)
	begin
		if rising_edge(clk_gen) then
			if ld_R = '1' then
				R <= C;
			end if;

			if reset = '1' then
				current_state <= INPUT_A;         -- Reset to initial state
			else
				current_state <= next_state;      -- go to next_state
			end if;
		end if;
	end process;

    -- Clock generation (simulation use only)
	process
	begin
		clk_gen <= '1', '0' after period/2;
		wait for period;
	end process;
	clk <= clk_gen;
end behaviour;

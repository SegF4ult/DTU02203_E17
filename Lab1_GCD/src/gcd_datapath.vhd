--
--  Title      :  Datapath Design for Structural implementation
--             :  of GCD
--             
--  Developers :  Xander Gregory Bos, Rafael Baron
--             
--  Purpose    :  This design contains the Datapath
--             
--  Note       :
--               
--  Revision   :  02203 fall 2017 v.1.0
--
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY gcd_datapath IS
    PORT (
   		-- top-level signals 
		   clk:		IN STD_LOGIC;
    		   reset:	IN STD_LOGIC;
		   AB:		IN  UNSIGNED(15 downto 0);
		   C	  : OUT unsigned(15 downto 0);
    		-- input from FSM
	           ABorALU:	IN  STD_LOGIC;
		   LDA:		IN  STD_LOGIC;
		   LDB:		IN  STD_LOGIC;
		   fn :		IN  std_logic_vector(1 downto 0);
    		-- output to FSM
		   N :		OUT  STD_LOGIC;
		   Z :		OUT  STD_LOGIC
		   );
END gcd_datapath;

ARCHITECTURE datapath OF gcd_datapath IS
COMPONENT mux IS
    PORT (data_in1:  IN  unsigned(15 DOWNTO 0);	-- Inputs.
          data_in2:  IN unsigned(15 DOWNTO 0);
          s       :  IN std_logic;				-- Select signal.
          data_out:  OUT  unsigned(15 DOWNTO 0)	-- Output.
          );
END COMPONENT;

COMPONENT reg IS
    PORT (clk:      IN  std_logic;				-- Clock signal.
          en:       IN  std_logic;				-- Enable signal.
          data_in:  IN  unsigned(15 DOWNTO 0);	-- Input data.
          data_out: OUT unsigned(15 DOWNTO 0));	-- Output data.
END COMPONENT;

COMPONENT alu IS
    PORT (A, B:     IN unsigned(15 DOWNTO 0);			-- Input operands.
          fn:       IN std_logic_vector(1 DOWNTO 0); 	-- Function.
          C:        OUT unsigned(15 DOWNTO 0);			-- Result.
          Z:        OUT std_logic;          			-- Result = 0 flag.
          N:        OUT std_logic);         			-- Result neg flag.
END COMPONENT;

COMPONENT tri IS
    PORT (data_in: IN unsigned(15 DOWNTO 0);
    	  data_out: OUT unsigned(15 DOWNTO 0));
END COMPONENT;

SIGNAL reg_a, reg_b, Y, C_int : unsigned(15 downto 0) := (others=> '0');

BEGIN
	
mux1: mux port map (data_in1=>AB, data_in2=>Y, s=>ABorALU, data_out=>C_int);
regA: reg port map (clk=>clk, en=>LDA, data_in=>C_int, data_out=>reg_a);
regB: reg port map (clk=>clk, en=>LDB, data_in=>C_int, data_out=>reg_b);
ALU1: ALU port map (A=>reg_a, B=>reg_b, fn=>fn, C=>Y, Z=>Z, N=>N);
tribuff: tri port map(data_in=>C_int, data_out=>C);

END datapath;

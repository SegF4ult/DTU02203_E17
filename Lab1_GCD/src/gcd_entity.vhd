-- -----------------------------------------------------------------------------
--
--  Title      :  FSMD implementation of GCD
--
--  Developers :  Jens Spars�, Rasmus Bo S�rensen and Mathias M�ller Bruhn
--
--  Purpose    :  This is a FSMD (finite state machine with datapath) 
--             :  implementation the GCD circuit
--
--  Revision   :  02203 fall 2017 v.4
--
--  Modified By: Xander Gregory Bos, Rafael Baron
--  Notes      : Separated entity declaration from architectures,
--		 to promote re-use and easy reimplementation of designs.
--
-- -----------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity gcd is
	port (clk:   in std_logic;			-- The clock signal.
	      reset: in std_logic;			-- Reset the module.
	      req:   in std_logic;			-- Input operand / Start computation.
	      AB:    in unsigned(15 downto 0);		-- The two operands.
	      ack:   out std_logic;			-- Computation is complete.
	      C:     out unsigned(15 downto 0));	-- The result.
end gcd;

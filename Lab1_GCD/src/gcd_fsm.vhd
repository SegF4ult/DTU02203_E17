--
--  Title      :  FSM Design for Structural implementation
--             :  of GCD
--             
--  Developers :  Xander Gregory Bos, Rafael Baron
--             
--  Purpose    :  This design contains the FSM
--             
--  Note       :
--               
--  Revision   :  02203 fall 2017 v.1.0
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity gcd_fsm is
  Port (
  -- Top-level signals
    clk: in std_logic;
    reset: in std_logic; 
    req: in std_logic;
    ack: out std_logic;
  -- Input from Datapath
    N: in std_logic;
    Z: in std_logic;
  -- Control to Datapath
    ABorALU: out std_logic;
    LDA: out std_logic;
    LDB: out std_logic;
    FN: out std_logic_vector(1 downto 0)
  );
end gcd_fsm;

architecture Behavioral of gcd_fsm is

type state_type is (INPUT_A, DONE_A, INPUT_B, CALC_C, DONE_ALL);
signal state, state_next : state_type := INPUT_A;
signal FN_reg, FN_next : std_logic_vector(1 downto 0) := "00";
-- FN "00" A-B
-- FN "01" B-A
-- FN "10" Pass A
-- FN "11" Pass B

begin

-- Registers
reg: process(clk, reset)
begin
	if reset='1' then
		state <= INPUT_A;
	elsif rising_edge(clk) then
		state <= state_next;
		FN <= FN_next;
		FN_reg <= FN_next;
	end if;
end process reg;

-- Combinatorial logic
cl: process(reset, req, N, Z)
begin
	-- Default signal assignments
	ABorALU <= '0';
	LDA <= '0';
	LDB <= '0';
	ack <= '0';
	state_next <= state;
	FN_next <= FN_reg;

	case state is
		when INPUT_A =>
			if req='1' then
				ack <= '1';
				LDA <= '1';
				state_next <= DONE_A;
			end if;

		when DONE_A =>
			if req='0' then
				state_next <= INPUT_B;
			end if;

		when INPUT_B =>
			if req='1' then
				state_next <= CALC_C;
				LDB <= '1';
--				ack<='1';
			end if;
		
		when CALC_C =>
			ABorALU <= '1';
		
			if FN_reg="00" and N='0' then
				LDA<='1';
			elsif FN_reg="00" and N='1' then
				FN_next <= "01";
			elsif FN_reg="01" and N='0' then
				LDB<='1';
			elsif FN_reg="01" and N='1' then
				FN_Next <= "00";
			end if;

			if Z='1' and FN_reg="00" then
				state_next <= DONE_ALL;
				FN_next <= "11"; -- PASS B;
				ack <= '1';
			elsif Z='1' and FN_reg="01" then
				state_next <= DONE_ALL;
				FN_next <= "10"; -- Pass A;
				ack <= '1';
			end if;

		when DONE_ALL =>
			ABorALU <= '1';
			ack <= '0';

			if req='0' then
				state_next <= INPUT_A;
			end if;
	end case;
end process cl;

end Behavioral;
--
--  Title      :  RTL-level FSMD design of the GCD component
--             :
--  Developers :  Xander Gregory Bos, Rafael Baron
--             :
--  Purpose    :  This design contains the FSM with Datapath 
--             :  used to implement Euler's GCD algorithm
--             :
--  Note       :  The design is modeled after a 2-process sequential model:
--             :  - One process handles register updates
--             :  - One process models the FSM and CL
--             :  
--  Revision   :  02203 fall 2017 v.1.1
--

architecture RTL of gcd is

type state_type is (INPUT_A, DONE_A, INPUT_B, CALC_C, DONE_ALL); -- Input your own state names

signal state, state_next : state_type;
signal reg_a, reg_a_next : unsigned(15 downto 0) := (others=> '0');
signal reg_b, reg_b_next : unsigned(15 downto 0) := (others=> '0');

begin

-- Registers

seq: process (clk, reset)
begin

	if reset='1' then
		state <= INPUT_A;
		reg_a <= (others=> '0');
		reg_b <= (others=> '0');
	elsif rising_edge(clk) then
		state <= state_next;
		reg_a <= reg_a_next;
		reg_b <= reg_b_next;
	end if;
	
end process seq;

-- Combinatorial logic

CL: process (req,AB,state,reg_a,reg_b,reset)
begin

    reg_a_next <= reg_a;
    reg_b_next <= reg_b;
    ack <= '0';
    C <= (others=>'0');

   case state is
	
	when INPUT_A =>
		if req = '1' then
			reg_a_next <= AB;
			ack <= '1';			
			state_next <= DONE_A;
		elsif req = '0' then
			state_next <= INPUT_A;
		end if;
	
	when DONE_A =>		
		if req = '0' then
			ack <= '0';
			state_next <= INPUT_B;
		elsif req = '1' then
			state_next <= DONE_A;
		end if;
	
	when INPUT_B =>
		if req='1' then
			reg_b_next <= AB;
			state_next <= CALC_C;
		else
			state_next <= INPUT_B;
		end if;	

	when CALC_C =>
		state_next <= CALC_C;

		if reg_a /= reg_b then
			if reg_a > reg_b then
				reg_a_next <= reg_a - reg_b;
			else
				reg_b_next <= reg_b - reg_a;
			end if;
		else
			state_next <= DONE_ALL;
		end if;
	-- it may be possible to remove the next_reg_x 
	
	when DONE_ALL =>
	    ack <= '1';
	    C <= reg_a;
		if req = '0' then
			state_next <= INPUT_A;
		elsif req='1' then
			state_next <= DONE_ALL;
		end if;

   end case;
end process CL;

end RTL;

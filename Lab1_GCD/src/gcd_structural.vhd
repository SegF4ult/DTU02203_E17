--
--  Title      :  Structural description for GCD
--             
--  Developers :  Xander Gregory Bos, Rafael Baron
--             
--  Purpose    :  This design uses submodules 
--             :  to implement Euler's GCD algorithm
--             
--  Note       :  
--               
--  Revision   :  02203 fall 2017 v.1.0
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

architecture structural of gcd is
    component gcd_fsm
        Port (
      -- Top-level signals
        clk: IN std_logic;
        reset: IN std_logic; 
        req: IN std_logic;
        ack: OUT std_logic;
      -- Input from Datapath
        N: IN std_logic;
        Z: IN std_logic;
      -- Control to Datapath
        ABorALU: OUT std_logic;
        LDA: OUT std_logic;
        LDB: OUT std_logic;
        FN: OUT std_logic_vector(1 downto 0)
      );
    end component;
    
    component gcd_datapath
        Port(
      -- Top-Level signals
      clk: IN std_logic;
      reset: IN std_logic;
      
      AB: IN unsigned(15 downto 0);
      C: OUT unsigned(15 downto 0);
      -- Output to Datapath
      N: OUT std_logic;
      Z: OUT std_logic;
    -- Control from FSM
      ABorALU: IN std_logic;
      LDA: IN std_logic;
      LDB: IN std_logic;
      FN: IN std_logic_vector(1 downto 0) 
        );
    end component;
    
    signal ABorALU, Neg, Zero, LDA, LDB: std_logic;
    signal FN: std_logic_vector(1 downto 0);

begin
    fsm : gcd_fsm PORT MAP (clk=>clk,  reset=>reset, req=>req, ack=>ack, N=>Neg, Z=>Zero, ABorALU=>ABorALU, LDA=>LDA, LDB=>LDB, FN=>FN);
    datapath: gcd_datapath PORT MAP (clk=>clk, reset=>reset, AB=>AB, C=>C, N=>Neg, Z=>Zero, ABorALU=>ABorALU, LDA=>LDA, LDB=>LDB, FN=>FN);

end structural;
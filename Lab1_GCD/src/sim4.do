
## ##############################################################
##
##	Title		: Compilation and simulation
##
##	Developers	: Jens Spars� and Rasmus Bo S�rensen
##
##	Revision	: 02203 fall 2012 v.2
##
## Script to set up the simulation of the project
#################################################################

# add wave will add the waves to the wave view
# To change the name of the waves in the wave view use the -label option
# Example:
# add wave -label clk /testbench/clk
add wave -noupdate -divider -height 24 Testbench
add wave /testbench/clk
add wave /testbench/reset
add wave -radix decimal /testbench/AB
# radix signal /testbench/AB decimal
add wave /testbench/req
add wave -radix decimal /testbench/C
# radix signal /testbench/C decimal
add wave /testbench/ack
add wave /testbench/environment/current_state
add wave /testbench/environment/next_state
add wave -noupdate -divider -height 24 FSM
add wave /testbench/GCD_module/fsm/ABorALU
add wave /testbench/GCD_module/fsm/LDA
add wave /testbench/GCD_module/fsm/LDB
add wave /testbench/GCD_module/fsm/FN_reg
add wave /testbench/GCD_module/fsm/FN_next
add wave -noupdate -divider -height 24 DataPath
add wave -radix decimal /testbench/GCD_module/datapath/regA/data_in
add wave -radix decimal /testbench/GCD_module/datapath/regA/data_out
add wave -radix decimal /testbench/GCD_module/datapath/regB/data_in
add wave -radix decimal /testbench/GCD_module/datapath/regB/data_out
add wave -radix decimal /testbench/GCD_module/datapath/C_int
# radix signal /testbench/GCD_module/datapath/C_int decimal
add wave /testbench/GCD_module/datapath/N
add wave /testbench/GCD_module/datapath/Z
add wave -radix decimal /testbench/GCD_module/datapath/ALU1/C

run 10000ns

# WaveRestoreZoom changes the wave view to show the simulated time
WaveRestoreZoom {0 ns} {5000 ns}

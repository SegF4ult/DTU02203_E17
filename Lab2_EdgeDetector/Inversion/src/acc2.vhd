-- -----------------------------------------------------------------------------
--
--  Title      :  Edge-Detection design project - task 2.
--             :
--  Developers :  Rafael Baron - rabarondtu.dk
--             :  Xander Gregory Bos - s170888@student.dtu.dk
--             :
--  Purpose    :  This design contains an entity for the accelerator that must be built
--             :  in task zero of the Edge Detection design project.
--             :
--  Revision   :  1.0   06-12-2017   Final version
--             :
--
-- -----------------------------------------------------------------------------

-- Entity
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.types.all;

entity acc is
    port(
        clk    : in  bit_t;             -- The clock.
        reset  : in  bit_t;             -- The reset signal. Active high.
        addr   : out halfword_t;        -- Address bus for data.
        dataR  : in  word_t;            -- The data bus.
        dataW  : out word_t;            -- The data bus.
        en     : out bit_t;             -- Request signal for data.
        we     : out bit_t;             -- Read/Write signal for data.
		start  : in  bit_t;				-- Signal to start processing.
		finish : out bit_t				-- Signal indicating work is done.
    );
end acc;

-- Architecture
architecture rtl of acc is

-- Custom types
type state_t is (IDLE, FETCH, STORE, DONE);
-- Constants
constant PIC1_START_ADDR : unsigned(15 downto 0) := x"0000";
constant PIC2_START_ADDR : unsigned(15 downto 0) := x"6300"; 
-- Internal signals/registers
signal state, state_next : state_t;
signal modified_word : word_t;
signal r_addr, r_addr_next : unsigned(15 downto 0);
signal w_addr, w_addr_next : unsigned(15 downto 0);

begin

-- Register Process
regs: process(clk, reset)
begin
	if reset = '1' then
		state <= IDLE;
		r_addr <= PIC1_START_ADDR;
		w_addr <= PIC2_START_ADDR;
	elsif rising_edge(clk) then
		state <= state_next;
		r_addr <= r_addr_next;
		w_addr <= w_addr_next;
	end if;
end process regs;

-- Operational Logic
logic: process(start, r_addr, w_addr, state, modified_word)
begin
-- Default assignments
---- regs
	state_next <= state;
	r_addr_next <= r_addr;
	w_addr_next <= w_addr;
---- external interface signals	
	finish <= '0';
	en <= '1'; -- Always assert memory enable signal
	we <= '0'; -- Memory operation is 'read' by default
	addr <= std_logic_vector(r_addr); -- Always pass the read address, unless overridden
	dataW <= modified_word; -- Writing output is always the current modified word

	case state is
		when IDLE =>
			if start = '1' then
				state_next <= FETCH;
			end if;
		when FETCH =>
			if r_addr < PIC2_START_ADDR then
				state_next <= STORE;
			else
				state_next <= DONE;
			end if;
		when STORE =>
			addr <= std_logic_vector(w_addr);
			we <= '1';
			state_next <= FETCH;
			r_addr_next <= r_addr + 1;
			w_addr_next <= w_addr + 1;
		when DONE =>
			finish <= '1';
			r_addr_next <= PIC1_START_ADDR;
			w_addr_next <= PIC2_START_ADDR;
			if start = '1' then
				state_next <= DONE;
			else
				state_next <= IDLE;
			end if;
	end case;
end process logic;

modified_word <= std_logic_vector(unsigned(word_one) - unsigned(dataR));

end rtl;

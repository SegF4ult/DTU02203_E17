-- -----------------------------------------------------------------------
--
--  Title      :  Edge-Detection design project - task 2.
--      
--  Developers :  Rafael Baron - rabaron@dtu.dk
--             :  Xander Gregory Bos - s170888@student.dtu.dk
--             :
--  Purpose    :  This design contains an entity for the accelerator that must be built
--             :  in task two of the Edge Detection design project.
--             :
--  Revision   :  1.0   06-12-2017   Final version
--             :
--
-- -----------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.types.all;

-- Entity
entity acc is
    port(
        clk    : in  bit_t;             -- The clock.
        reset  : in  bit_t;             -- The reset signal. Active high.
        addr   : out halfword_t;        -- Address bus for data.
        dataR  : in  word_t;            -- The data bus.
        dataW  : out word_t;            -- The data bus.
        en     : out bit_t;             -- Request signal for data.
        we     : out bit_t;             -- Read/Write signal for data.
		start  : in  bit_t;				-- Signal to start processing.
		finish : out bit_t				-- Signal indicating work is done.
    );
end acc;

-- Architecture
architecture rtl of acc is

-- Custom types
TYPE state_type IS (IDLE, READ_MEM, REG_SHIFT, LOAD_REG, CALC_CONV, WRITE_MEM, DONE);

-- Constants
CONSTANT CONST_IMG_SIZE_WORDS		: unsigned(15 downto 0) := x"6300"; -- Size of image in words
CONSTANT CONST_IMG_SRC_START_ADDR	: unsigned(15 downto 0) := x"0000"; -- Start address of source image
CONSTANT CONST_IMG_DST_START_ADDR	: unsigned(15 downto 0) := x"635A"; -- Start address of destination image, offset by 5A due to the fact that the border is not processed
CONSTANT CONST_REG_COUNT			: unsigned(7 downto 0) := x"B3"; -- Amount of words to read during initialization

-- All internal signals are defined here
-- -- State & Memory
SIGNAL state, state_next : state_type;
SIGNAL read_addr, read_addr_next : unsigned(15 downto 0);
SIGNAL write_addr, write_addr_next : unsigned(15 downto 0);
-- -- Data & Counters
SIGNAL data_reg, data_reg_next: unsigned(5727 downto 0); 
SIGNAL reg_counter, reg_counter_next : unsigned(7 downto 0) := x"00";
SIGNAL word_counter, word_counter_next: unsigned(15 downto 0) := x"0000";
-- -- Image convolution
SIGNAL conv_pixels, conv_pixels_next : unsigned(31 downto 0);
SIGNAL Dx1, Dy1, Dx2, Dy2, Dx3, Dy3, Dx4, Dy4 : signed(8 downto 0);

begin

seq: process(clk,reset)
begin
	if reset='1' then
		state <= IDLE;
		read_addr <= CONST_IMG_SRC_START_ADDR;
		write_addr <= CONST_IMG_DST_START_ADDR;
		data_reg <= (others => '0');
		conv_pixels <= (others => '0');
		reg_counter <= x"00";
		word_counter <= x"0000";
	elsif rising_edge(clk) then
		state <= state_next;
		read_addr <= read_addr_next;
		write_addr <= write_addr_next;
		data_reg <= data_reg_next;
		conv_pixels <= conv_pixels_next;
		reg_counter <= reg_counter_next;
		word_counter <= word_counter_next; --this is the counter to check the end of the file
	end if;
end process seq;

sobel: PROCESS (start, state, read_addr, write_addr, reg_counter, word_counter, data_reg, dataR, Dx1, Dx2, Dx3, Dx4, Dy1, Dy2, Dy3, Dy4, conv_pixels)
BEGIN
-- Default assignments
-- -- Outbound
	en <= '1';
	we <= '0';
	finish <= '0';
	addr <= std_logic_vector(read_addr);
    dataW <= std_logic_vector(conv_pixels);
-- -- Registers
	state_next <= state;
	read_addr_next <= read_addr;
	write_addr_next <= write_addr;
	reg_counter_next <= reg_counter;
	word_counter_next <= word_counter;
	data_reg_next <= data_reg;
	conv_pixels_next <= conv_pixels;
-- -- Calculations
	Dx1 <= signed('0'& data_reg(2839 downto 2832)) - signed('0'& data_reg(2823 downto 2816));
    Dx2 <= signed('0'& data_reg(2847 downto 2840)) - signed('0'& data_reg(2831 downto 2824));
    Dx3 <= signed('0'& data_reg(2855 downto 2848)) - signed('0'& data_reg(2839 downto 2832));
    Dx4 <= signed('0'& data_reg(2863 downto 2856)) - signed('0'& data_reg(2847 downto 2840));
    Dy1 <= signed('0'& data_reg(07 downto 00)) - signed('0'& data_reg(5647 downto 5640));
    Dy2 <= signed('0'& data_reg(15 downto 08)) - signed('0'& data_reg(5655 downto 5648));
    Dy3 <= signed('0'& data_reg(23 downto 16)) - signed('0'& data_reg(5663 downto 5656));
    Dy4 <= signed('0'& data_reg(31 downto 24)) - signed('0'& data_reg(5671 downto 5664));

	CASE state IS

    when IDLE =>
		read_addr_next <= CONST_IMG_SRC_START_ADDR;
		write_addr_next <= CONST_IMG_DST_START_ADDR;
		data_reg_next <= (others => '0');
		reg_counter_next <= x"00";
		word_counter_next <= x"0000";
		if start = '1' then
			state_next <= READ_MEM;
		end if;

	when READ_MEM =>
        state_next <= LOAD_REG;

	when LOAD_REG =>
        if reg_counter < CONST_REG_COUNT then -- count until CONST_REG_COUNT, which is the number of words to have in the data_reg
            data_reg_next(5727 downto 5696) <= unsigned(dataR);
            state_next <= REG_SHIFT;
        else
            state_next <= CALC_CONV;
        end if;

    when REG_SHIFT =>
        state_next <= READ_MEM;
        data_reg_next(5695 downto 0) <= data_reg(5727 downto 32); -- shifts the register content
        reg_counter_next <= reg_counter+1;
        word_counter_next <= word_counter+1;
        read_addr_next <= read_addr+1;

	when CALC_CONV =>
		state_next <= WRITE_MEM;

        conv_pixels_next(7 downto 0) <= unsigned(abs(Dx1(8 downto 1))) + unsigned(abs(Dy1(8 downto 1)));
        conv_pixels_next(15 downto 8) <= unsigned(abs(Dx2(8 downto 1))) + unsigned(abs(Dy2(8 downto 1)));
        conv_pixels_next(23 downto 16) <= unsigned(abs(Dx3(8 downto 1))) + unsigned(abs(Dy3(8 downto 1)));
        conv_pixels_next(31 downto 24) <= unsigned(abs(Dx4(8 downto 1))) + unsigned(abs(Dy4(8 downto 1)));
        reg_counter_next <= reg_counter-1;

	when WRITE_MEM =>
		state_next <= READ_MEM;
		we<='1';
		addr <= std_logic_vector(write_addr);
		write_addr_next <= write_addr + 1;
		if word_counter > CONST_IMG_SIZE_WORDS then
			state_next <= DONE;
		end if;

	when DONE =>
		finish <= '1';
		if start = '0' then
			state_next <= IDLE;
		end if;
	END CASE;

end process sobel;
end;

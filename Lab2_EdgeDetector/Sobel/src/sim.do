
## ##############################################################
##
##	Title		: Compilation and simulation
##
##	Developers	: 
##
##	Revision	: 
##
## Script to set up the simulation of the project
#################################################################

# add wave will add the waves to the wave view
# To change the name of the waves in the wave view use the -label option
# Example:
# add wave -label clk /testbench/clk
add wave /testbench/clk
add wave /testbench/addr
add wave /testbench/dataR
add wave /testbench/dataW
add wave /testbench/start
add wave /testbench/reset
add wave /testbench/en
add wave /testbench/we
add wave /testbench/Accelerator/counter
add wave /testbench/Accelerator/state
add wave /testbench/Accelerator/state_next
add wave /testbench/Accelerator/counter_file
add wave /testbench/finish

run 1000ns

# WaveRestoreZoom changes the wave view to show the simulated time
WaveRestoreZoom {0 ns} {5000 ns}
